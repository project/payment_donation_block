<?php

namespace Drupal\payment_donation_block\Plugin\Payment\Type;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\payment\EventDispatcherInterface;
use Drupal\payment\Plugin\Payment\Type\PaymentTypeBase;
use Drupal\payment\Response\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * The payment donation block payment type.
 *
 * @PaymentType(
 *   id = "payment_donation_block",
 *   label = @Translation("Payment donation block")
 * )
 */
class PaymentDonationBlockPaymentType extends PaymentTypeBase implements ContainerFactoryPluginInterface {
  /**
   * Include the messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new instance.
   *
   * @param mixed[] $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed[] $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\payment\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translator.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger interface.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, EventDispatcherInterface $event_dispatcher, TranslationInterface $string_translation, MessengerInterface $messenger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $event_dispatcher);
    $this->stringTranslation = $string_translation;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('payment.event_dispatcher'),
      $container->get('string_translation'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentDescription() {
    return $this->t('Payment donation block');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'destination_url' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  protected function doGetResumeContextResponse() {
    $payment = $this->getPayment();
    $payment_status = $payment->getPaymentStatus()->getPluginId();
    switch ($payment_status) {
      case 'payment_success':
        $this->messenger->addMessage($this->t('Thank you for your donation!'));
        break;

      case 'payment_cancelled':
        $this->messenger->addMessage($this->t('You cancelled the payment. Please try again.'));
        break;

      case 'payment_authorization_failed':
      case 'payment_pending':
        $this->messenger->addMessage($this->t('The payment is pending.'));
        break;

      default:
        $this->messenger->addMessage($this->t('Something went wrong. Please contact us.'));
    }
    return new Response(Url::fromUri($this->getDestinationUrl()));
  }

  /**
   * {@inheritdoc}
   */
  public function resumeContextAccess(AccountInterface $account) {
    return AccessResult::allowed();
  }

  /**
   * Sets the URL the user should be redirected to upon resuming the context.
   *
   * @param string $url
   *   The destination URL.
   *
   * @return $this
   */
  public function setDestinationUrl($url) {
    $this->configuration['destination_url'] = $url;

    return $this;
  }

  /**
   * Gets the URL the user should be redirected to upon resuming the context.
   *
   * @return string
   *   The destination URL.
   */
  public function getDestinationUrl() {
    return $this->configuration['destination_url'];
  }

}
