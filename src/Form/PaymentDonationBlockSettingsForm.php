<?php

namespace Drupal\payment_donation_block\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\payment\Plugin\Payment\Method\PaymentMethodManager;
use Drupal\currency\FormHelper;

/**
 * Configure the Payment donations form.
 */
class PaymentDonationBlockSettingsForm extends ConfigFormBase {

  /**
   * The payment method manager.
   *
   * @var \Drupal\payment\Plugin\Payment\Method\PaymentMethodManager
   */
  protected $paymentMethodManager;

  /**
   * The currency form helper.
   *
   * @var \Drupal\currency\FormHelper
   */
  protected $currencyFormHelper;

  /**
   * Construct the class.
   *
   * @param \Drupal\payment\Plugin\Payment\Method\PaymentMethodManager $payment_method_manager
   *   The payment method manager.
   * @param \Drupal\currency\FormHelper $currency_form_helper
   *   The currency form helper.
   */
  public function __construct(
    PaymentMethodManager $payment_method_manager,
    FormHelper $currency_form_helper) {
    $this->paymentMethodManager = $payment_method_manager;
    $this->currencyFormHelper = $currency_form_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.payment.method'),
      $container->get('currency.form_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'payment_donation_block.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'payment_donation_block_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('payment_donation_block.settings');

    $payment_methods = [];
    foreach ($this->paymentMethodManager->getDefinitions() as $definition) {
      if ($definition['active']) {
        $payment_methods[$definition['id']] = $definition['label'];
      }
    }

    $form['payment_method'] = [
      '#type' => 'select',
      '#title' => $this->t('Payment method'),
      '#description' => $this->t('Select the payment method you want to use for donations.'),
      '#options' => $payment_methods,
      '#default_value' => $config->get('payment_method'),
      '#required' => TRUE,
    ];

    $form['currency_code'] = [
      '#type' => 'select',
      '#title' => $this->t('Payment currency'),
      '#options' => $this->currencyFormHelper->getCurrencyOptions(),
      '#default_value' => $config->get('currency_code'),
      '#required' => TRUE,
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('The text shown above the donation form.'),
      '#default_value' => $config->get('description'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('payment_donation_block.settings')
      ->set('payment_method', $form_state->getValue('payment_method'))
      ->set('currency_code', $form_state->getValue('currency_code'))
      ->set('description', $form_state->getValue('description'))
      ->save();
  }

}
