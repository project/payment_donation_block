<?php

namespace Drupal\payment_donation_block\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\payment\Entity\Payment;
use Drupal\payment\Plugin\Payment\LineItem\PaymentLineItemManagerInterface;
use Drupal\payment\Plugin\Payment\Method\PaymentMethodManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * The donation form.
 */
class PaymentDonationForm extends FormBase {
  /**
   * Include the messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The payment method manager.
   *
   * @var \Drupal\payment\Plugin\Payment\Method\PaymentMethodManagerInterface
   */
  protected $paymentMethodManager;

  /**
   * The payment line item manager.
   *
   * @var \Drupal\payment\Plugin\Payment\LineItem\PaymentLineItemManagerInterface
   */
  protected $paymentLineItemManager;

  /**
   * Construct the class.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger interface.
   * @param \Drupal\payment\Plugin\Payment\Method\PaymentMethodManagerInterface $payment_method_manager
   *   The payment method manager.
   * @param \Drupal\payment\Plugin\Payment\LineItem\PaymentLineItemManagerInterface $payment_line_item_manager
   *   The payment line item manager.
   */
  public function __construct(MessengerInterface $messenger, PaymentMethodManagerInterface $payment_method_manager, PaymentLineItemManagerInterface $payment_line_item_manager) {
    $this->messenger = $messenger;
    $this->paymentMethodManager = $payment_method_manager;
    $this->paymentLineItemManager = $payment_line_item_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('plugin.manager.payment.method'),
      $container->get('plugin.manager.payment.line_item')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'payment_donation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('payment_donation_block.settings');

    $form['description'] = [
      '#markup' => '<p>' . $config->get('description') . '</p>',
    ];

    $form['amount'] = [
      '#type' => 'number',
      '#title' => $this->t('Donation amount'),
      '#required' => TRUE,
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Donate'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!is_numeric($form_state->getValue('amount'))) {
      $form_state->setErrorByName(
        'amount',
        $this->t('This must be a number.')
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('payment_donation_block.settings');
    $payment = Payment::create(['bundle' => 'payment_donation_block']);
    $payment->setPaymentMethod($this->paymentMethodManager->createInstance($config->get('payment_method')));

    $payment->setCurrencyCode($config->get('currency_code'));
    $payment_type = $payment->getPaymentType();
    $payment_type->setDestinationUrl($this->getRequest()->getUri());

    $line_items = [
      $this->paymentLineItemManager->createInstance('payment_basic', [])
        ->setName('Test name')
        ->setAmount($form_state->getValue('amount'))
        ->setQuantity(1)
        ->setCurrencyCode($config->get('currency_code'))
        ->setDescription('Test description'),
    ];
    $payment->setLineItems($line_items);
    $payment->save();
    $payment->execute();
  }

}
