# Payment donation block

## CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration

## INTRODUCTION

A simple module that provides a block with a form to help sites accept
donations on their drupal site with payment-integration.

## REQUIREMENTS

This module requires the payment module to be setup, with a working
payment provider.

## INSTALLATION

 * Install as you would normally install a contributed Drupal module.
   Visit: https://www.drupal.org/node/1897420 for further information.
 * Configure the module.

## CONFIGURATION
1. Navigate to "Adminstration > Configuration > Web Services >
   payment Donation Block.
   1. Configure payment method.
   2. Configure currency.
2. Navigate to Structure > Block Layout and place "payment Donation Block"
   block on a region on your page.
